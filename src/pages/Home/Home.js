import React, { Component } from 'react'
import { ScrollView, Text, View } from 'react-native'
import { apiGlobal } from '../../config'
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Header from '../../components/header/Header';
import MainCardInfo from '../../components/mainCardInfo/MainCardInfo'
import CardInfo from '../../components/mainCardInfo/CardInfo'
import Loading from '../../components/loadings/Loading'
import LoadingMainInfo from '../../components/loadings/LoadingMainInfo'
import LoadingIndo from '../../components/loadings/LoadingIndo';
export class Home extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            deaths: 0,
            confirmed: 0,
            recovered: 0,
            loading : false,
        }
    }

    componentDidMount(){
        this.getData()
    }

    
    getData = () => {
        this.setState({loading: false})
        apiGlobal.get().then(hasil => {
            console.log(hasil.data.deaths.value);
            this.setState({
                deaths: hasil.data.deaths.value,
                confirmed: hasil.data.confirmed.value,
                recovered: hasil.data.recovered.value,
                loading: true
            })
        }).catch((e) => {
            this.state({loading: false})
        })
    }

    render() {
        const {deaths,confirmed,recovered,loading} = this.state;
        return (
            <ScrollView >
                <View style={{flex:1,alignItems:'center'}}>
                    {/* header */}
                    <Header />
                    {/* card info */}
                    <View style={{width:'90%',height:200,position:'relative',marginTop:-40, backgroundColor:'green'}} >
                        {
                            loading ?
                            <LoadingIndo />
                            :
                            <MainCardInfo nilai={deaths.value} />
                        }
                        {
                            loading ?
                            <View style={{width:'100%',height:75,flexDirection:'row'}} >
                                <Loading mr={6} />
                                <Loading ml={6} />
                            </View>
                            :
                            <View style={{width:'100%',height:75,flexDirection:'row'}} >
                                <CardInfo nilai={confirmed.value} title={'TOTAL POSITIF'} warna={'orange'} fontSize={14} mr={6} />
                                <CardInfo nilai={recovered.value} title={'TOTAL SEMBUH'} warna={'green'} fontSize={14} ml={6} />
                            </View>

                        }
                    </View>
                    {/* text intruksi */}
                </View>
                {
                    loading ? 
                    <View>
                        <Text> {deaths} </Text>
                        <Text> {confirmed} </Text>
                        <Text> {recovered} </Text>
                    </View> 
                    :
                    <SkeletonPlaceholder>
                            <SkeletonPlaceholder.Item marginLeft={20}>
                            <SkeletonPlaceholder.Item width={170} height={15} borderRadius={4} />
                            <SkeletonPlaceholder.Item width={170} height={15} borderRadius={4} marginTop={6} />
                            <SkeletonPlaceholder.Item
                                marginTop={6}
                                width={80}
                                height={15}
                                borderRadius={4}
                            />
                            </SkeletonPlaceholder.Item>
                    </SkeletonPlaceholder>


                }
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
                <Text> textInComponent </Text>
            </ScrollView>
        )
    }
}

export default Home

