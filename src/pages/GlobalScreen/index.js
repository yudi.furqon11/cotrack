import React, { Component } from 'react'
import { ScrollView, View,Text, Image } from 'react-native'
import Header from '../../components/header/Header'
import MainCardInfo from '../../components/mainCardInfo/MainCardInfo'
import { apiGlobal } from '../../config'
import CardInfo from '../../components/mainCardInfo/CardInfo'
import Loading from '../../components/loadings/Loading'
import LoadingMainInfo from '../../components/loadings/LoadingMainInfo'
import CardIntruksiMol from '../../moleculs/CardIntruksiMol'

class GlobalScreen extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            deaths: 0,
            confirmed: 0,
            recovered: 0,
            loading: false
        }
    }

    componentDidMount(){
        this.getData()
    }

    
    getData = () => {
        this.setState({loading: true})
        apiGlobal.get().then(hasil => {
            this.setState({
                deaths: hasil.data.deaths,
                confirmed: hasil.data.confirmed,
                recovered: hasil.data.recovered,
                loading: false
            })
        }).catch((e) => {
            this.setState({loading: true})
        })
    }
    render() {
        const {deaths,recovered,confirmed,loading} = this.state;
        return (
            <ScrollView showsVerticalScrollIndicator={false} style={{backgroundColor:'#fff'}}>
                <View style={{flex:1,alignItems:'center',height:'100%'}}>
                    {/* header */}
                    <Header subtitle={"Global Data"} />
                    {/* card info */}
                    <View style={{width:'90%',height:200,marginTop:-40}} >
                        {
                            loading ?
                            <LoadingMainInfo />
                            :
                            <MainCardInfo nilai={deaths.value} />
                        }
                        {
                            loading ?
                            <View style={{width:'100%',height:75,flexDirection:'row'}} >
                                <Loading mr={6} />
                                <Loading ml={6} />
                            </View>
                            :
                            <View style={{width:'100%',height:75,flexDirection:'row'}} >
                                <CardInfo nilai={confirmed.value} title={'TOTAL POSITIF'} warna={'orange'} fontSize={14} mr={6} />
                                <CardInfo nilai={recovered.value} title={'TOTAL SEMBUH'} warna={'green'} fontSize={14} ml={6} />
                            </View>

                        }
                    </View>
                </View>
                {/* text intruksi */}
                <View style={{flex:1,paddingHorizontal:20}}>
                    <Text style={{
                        fontSize:22,fontWeight: "700",color:'#3A1B7A',}} >
                        AYO LAWAN COVID-19 !!!
                    </Text>
                    <Text style={{
                        fontSize:17,color:'#929292',}} >
                        hindari persebaran covid-19 dengan cara 5M.
                    </Text>
                </View>
                {/* card intruksi */}
                <CardIntruksiMol />
            </ScrollView>
        )
    }
}

export default GlobalScreen
