import React from 'react'
import { View, Text, Image, ScrollView } from 'react-native'

const MainCardInfo = (props) => {
    return (
        <View style={{
            width:'100%',
            height:90,
            backgroundColor:'#fff',
            borderRadius:5,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
            elevation: 6,
            justifyContent:'center',
            alignItems:'center',
            marginBottom:10,
            flexDirection:'row'
        }} >
            <View style={{flex:1, justifyContent:'center',alignItems:'center'}} >
                <Text style={{color:'red', fontWeight:'700',fontSize:14}} >TOTAL MENINGGAL</Text>
                <Text style={{color:'red', fontWeight:'700',fontSize:26,marginVertical:-7}} >{props.nilai}</Text>
                <Text style={{color:'red', fontWeight:'700',fontSize:12}} >Orang</Text>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                <Image style={{width:200,height:110,position:'absolute',right:10,top:-65}} source={require('../../assets/stats2.png')} />
            </View>
        </View>
    )
}

export default MainCardInfo
