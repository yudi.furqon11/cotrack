import React from 'react'
import { View, Text } from 'react-native'

const CardProv = (props) => {
    return (
        <View style={{
            marginBottom:7,
            height:50,
            flex:1,
            flexDirection:'row',
            backgroundColor:'#fff',
            borderRadius:5,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
            elevation: 4,
            paddingHorizontal:10
        }}>
            <View style={{flex:2,justifyContent:'center',alignItems:'flex-start'}}>
                <Text style={{fontWeight:'bold'}}>{props.prov}</Text>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
                <Text onPress={props.onPress} style={{fontSize:11,fontWeight:'bold',color:'#2400FF'}}>Lihat Detail</Text>
            </View>
        </View>
    )
}

export default CardProv
