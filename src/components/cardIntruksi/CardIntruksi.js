import React from 'react'
import { View, Text,Image } from 'react-native'

const CardIntruksi = (props) => {
    return (
        <View  style={{flex:1,flexDirection:'row',height:170,borderRadius:10,backgroundColor:props.warnaBg,marginBottom:15}} >
            <View style={{flex:1.5,padding:20}}>
                <Text style={{color:props.warnaTitle,fontWeight:'bold',fontSize:16,marginBottom:10}}>
                    {props.title}
                </Text>
                <Text style={{color:props.warnaDes,fontSize:11,fontWeight: '700'}}>
                    {props.deskripsi}
                </Text>
            </View>
            <View style={{flex:.8,justifyContent:'center',alignItems:'center'}}>
                <Image style={{width:props.w,height:props.h,marginRight:props.mr,marginTop:props.mt}} source={props.img} />
            </View>
        </View>
    )
}

export default CardIntruksi
