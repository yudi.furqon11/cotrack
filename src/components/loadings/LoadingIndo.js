import React from 'react'
import { View, Text } from 'react-native'
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

const LoadingIndo = (props) => {
    return (
        <View style={{flexDirection:'row',marginBottom:10}}>
            <View style={{flex:1.5}}>
                <SkeletonPlaceholder>
                    <SkeletonPlaceholder.Item >
                    <SkeletonPlaceholder.Item width={170} height={15} borderRadius={4} />
                    <SkeletonPlaceholder.Item width={100} height={20} borderRadius={4} marginTop={5} />
                    <SkeletonPlaceholder.Item width={140} height={15} borderRadius={4} marginTop={5} />
                    </SkeletonPlaceholder.Item>
                </SkeletonPlaceholder>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <SkeletonPlaceholder>
                    <SkeletonPlaceholder.Item alignItems={'center'} >
                    <SkeletonPlaceholder.Item width={70} height={70} borderRadius={50} />
                    </SkeletonPlaceholder.Item>
                </SkeletonPlaceholder>
            </View>
        </View>
    )
}

export default LoadingIndo
