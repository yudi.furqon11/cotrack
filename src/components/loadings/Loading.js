import React from 'react'
import { View, Text } from 'react-native'
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

const Loading = (props) => {
    return (
        <View style={{
            flex:1,
            backgroundColor:'#fff',
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
            elevation: 6,
            justifyContent:'center',
            alignItems:'center',
            marginLeft:props.ml,
            marginRight:props.mr,
            borderRadius:5,
        }} >
        <SkeletonPlaceholder>
            <SkeletonPlaceholder.Item >
            <SkeletonPlaceholder.Item width={120} height={15} borderRadius={4} />
            <SkeletonPlaceholder.Item width={100} height={15} borderRadius={4} marginTop={5} />
            </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder>
        </View>
    )
}

export default Loading
