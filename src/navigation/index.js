import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../pages/Home/Home';
import GlobalScreen from '../pages/GlobalScreen';
import Splash from '../pages/splash/Splash';
import IndonesiaScreen from '../pages/IndonesiaScreen';

const Menu = createStackNavigator();
const TabMenu = createBottomTabNavigator();

const TabScreen = () => {
  return (
    <TabMenu.Navigator>
      <TabMenu.Screen name="Global" component={GlobalScreen} />
      <TabMenu.Screen name="Indonesia" component={IndonesiaScreen} />
      <TabMenu.Screen name="Home" component={Home} />
    </TabMenu.Navigator>
  );
};

const Route = () => {
  return (
    <Menu.Navigator>
      <Menu.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Menu.Screen
        name="GlobalScreen"
        component={TabScreen}
        options={{headerShown: false}}
      />
      <Menu.Screen
        name="IndonesiaScreen"
        component={IndonesiaScreen}
        options={{headerShown: false}}
      />
      <Menu.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
    </Menu.Navigator>
  );
};

export default Route;